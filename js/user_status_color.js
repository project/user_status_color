Drupal.behaviors.user_status_color_edit_status_form = function() {
  $("#user-status-color-edit-status-form .user-status-color-operation a.op-delete").unbind('click').click( function() {
     var answer = confirm('Are you sure you whant to delete ' + $(this).parent().parent().find('input.user_status').val() + ' status?');
     if (answer) {
       delete_status($(this).parent().parent().find('input.user_status').val());
       $(this).parent().parent().hide();
     }
    return false;
  });
  $("#user-status-color-edit-status-form .user-status-color-operation a.op-edit").unbind('click').click( function() {
     $(this).parent().parent().find("td.tdinactive").addClass('tdactive');
     $(this).parent().parent().find("td.tdinactive").removeClass('tdinactive');
     $(this).parent().parent().find('input[type=text]').removeAttr('readonly');
     $(this).hide();
     $(this).parent().find('a.op-save').css('display', 'inline-block');
    return false;
  });
  $("#user-status-color-edit-status-form .user-status-color-operation a.op-save").unbind('click').click( function() {
    save_changes($(this).parent().find("input[name=old_status]").val(), $(this).parent().parent().find("input[name=status]").val(), $(this).parent().parent().find("input[name=color]").val().substr(1, 6), $(this).parent().parent().find("input[name=message]").val());
    return false;
  });
  $('#user-status-color-edit-status-form input[type=text]').keypress(function(event) {
   if(event.which == 13) {
     save_changes($(this).parent().parent().find("input[name=old_status]").val(), $(this).parent().parent().find("input[name=status]").val(), $(this).parent().parent().find("input[name=color]").val().substr(1, 6), $(this).parent().parent().find("input[name=message]").val());
     return false;
   }
  });
}

function save_changes(oldStatus, newStatus, color, message) {
  if (!oldStatus || !newStatus || !color || !message) {
    $('td.tdactive').find('input[type=text]').addClass('error');
  }
  else {
    $.ajax({
      url: "/admin/status/edit/color?oldStatus=" + oldStatus + "&newStatus=" + newStatus + "&message=" + message + "&color=" + color,
      dataType: 'json',
      success: function(response) {
        console.log(response);
        if (response.status != 1) {
          $('td.tdactive').find('input[type=text]').addClass('error');
        }
        else{
           var container = $('td.tdactive').parent();
           container.find('.user-status-color-operation a.op-save').hide();
           container.find('.user-status-color-operation a.op-edit').show();
           container.find('input[type=text]').removeClass('error');
           container.find('input[type=text]').attr('readonly', 'readonly');
           $('td.tdactive').addClass('tdinactive');
           $('td.tdactive').removeClass('tdactive');
        }
      }
    });
  }
  Drupal.attachBehaviors();
}

function delete_status(status) {
  $.ajax({
    url: "/admin/status/delete/" + status,
    dataType: 'json',
    success: function(data) {
    }
  });
}
<?php
/**
 * @file
 * User status Administrative Interface
 *
 * @author Gabriel Ungureanu
 * gabriel.ungreanu@ag-prime.com
 * http://twitter.com/gabiu
 */

/**
 * Function display a list with all statuses
 * @return string
 */
function user_status_color_settings() {
  $path = drupal_get_path('module', 'user_status_color');

  // Add our custom js and css for our colorpicker
  drupal_add_js("$path/js/user_status_color.js");
  drupal_add_css("$path/css/user_status_color.css");

  $output = "<div class = 'container-user-status-color'>";
  $output .= user_status_color_get_status_table();
  $output .= drupal_get_form('user_status_color_form_add');
  $output .= "</div>";
  return $output;
}

/**
 * Add new status color form
 */
function user_status_color_form_add() {
  $form = array();
  $status_table = user_status_color_get_status_table();
  $form['user_status_add_container'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add new status'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );
  $form['user_status_add_container']['user_status_color_status'] = array(
    '#type' => 'textfield',
    '#title' => t('Status'),
    '#description' => t("Status name is usually the color name (Black, Green, Orange, etc.), but can be any other status name you wish to assign."),
  );
  $form['user_status_add_container']['user_status_color_color'] = array(
    '#type' => 'colorpicker_textfield',
    '#title' => t('Color'),
    '#default_value' => '#000000',
  );
  $form['user_status_add_container']['user_status_color_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Message'),
    '#description' => t("Message is the default message for that color.")
  );
  $form['user_status_add_container']['user_status_color_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  $form['#validate'] = array('user_status_color_settings_validate');
  $form['#submit'] = array('user_status_color_settings_submit');
  return $form;
}

/**
 * Validate function for add new status color form
 * @param <array> $form
 * @param <array> $form_state
 */
function user_status_color_settings_validate($form, &$form_state) {
  if (empty($form_state['values']['user_status_color_status'])) {
    form_set_error('user_status_color_status', t('Status cannot be blank.'));
    return FALSE;
  }
  if (empty($form_state['values']['user_status_color_message'])) {
    form_set_error('user_status_color_message', t('Message cannot be blank.'));
    return FALSE;
  }
  return TRUE;
}

/**
 * Submit function for add new status color form
 * @param <array> $form
 * @param <array> $form_state
 */
function user_status_color_settings_submit($form, &$form_state) {
  if (isset($form_state['values']['user_status_color_status']) && isset($form_state['values']['user_status_color_message']) && isset($form_state['values']['user_status_color_color'])) {
    $status[$form_state['values']['user_status_color_status']] = array(
      "color" => $form_state['values']['user_status_color_color'],
      "message" => $form_state['values']['user_status_color_message']
    );
  }
  $statuses = variable_get('user_status_color_list', USER_STATUS_COLOR_DEFAULT);
  if (is_array($statuses) && !empty($statuses)) {
     $save_status = array_merge($statuses, $status);
  }
  else {
    $save_status = $status;
  }
  variable_set('user_status_color_list', $save_status);
}

/**
 * This function return a table with all statuses
 * @return string
 */
function user_status_color_get_status_table() {
  $statuses = variable_get('user_status_color_list', USER_STATUS_COLOR_DEFAULT);
  if (!empty($statuses)) {
    $header = array(
      t('Status'),
      t('Color'),
      t('Message'),
      t('Operations')
    );
    $dispaly_statuses = array();
    foreach ($statuses as $status => $status_data) {
      $dispaly_statuses[] = array(
        array('data' => "<input type='textfield' value='$status' name='status' readonly class='user_status'>", 'class' => 'tdinactive'),
        array('data' => "<input type='textfield' value ='" . $status_data['color'] . "' name='color' class='colorpicker_textfield' readonly><div class='picker_wrapper'></div>", 'class' => 'tdinactive'),
        array('data' => "<input type='textfield' value='" . $status_data['message'] . "' name='message' readonly>", 'class' => 'tdinactive'),
        array('data' => l('', '', array('attributes' => array('class' => 'op-edit'))) . l('', '', array('attributes' => array('class' => 'op-save'))) . l('', '', array('attributes' => array('class' => 'op-delete'))) . "<input type='hidden' name='old_status' value='$status'>", 'class' => 'user-status-color-operation')
      );
    }
    $status_table = theme_table($header, $dispaly_statuses);
    $edit_form = drupal_get_form('user_status_color_edit_status_form', $status_table);
  }
  return $edit_form;
}

/**
 * Edit status form
 * @param <type> $form
 * @param <type> $table
 * @return string
 */
function user_status_color_edit_status_form($form, $table) {
  $form = array();
  $form['status_list'] = array(
    '#type' => 'markup',
    '#value' => $table
  );
  return $form;
}

/**
 * This function is use by an ajax call to edit an existing status.
 */
function user_status_color_edit() {
  $return_status = array();
  if (!$_GET['oldStatus'] || !$_GET['newStatus'] || !$_GET['color'] || !$_GET['message']) {
    $return_status = array(
      'status' => '0',
      'message' => t("Can't save the changes!"),
    );
  }
  else {
    $statuses = variable_get('user_status_color_list', USER_STATUS_COLOR_DEFAULT);
    unset($statuses[$_GET['oldStatus']]);
    $statuses[$_GET['newStatus']] = array(
      'color' => '#' . $_GET['color'],
      'message' => $_GET['message']
    );
    variable_set('user_status_color_list', $statuses);
    $return_status = array(
      'status' => '1',
      'message' => t('Status was saved!'),
    );
  }
  echo json_encode($return_status);
  die();
}

/**
 * This function is use by an ajax call to delete an existing status
 * @param <type> $delete_status
 */
function user_status_color_delete($delete_status) {
  $statuses = variable_get('user_status_color_list', USER_STATUS_COLOR_DEFAULT);
  if (is_array($statuses)) {
    foreach ($statuses as $status => $status_data) {
      if ($status == $delete_status) {
        unset($statuses[$status]);
      }
    }
    variable_set('user_status_color_list', $statuses);
  }
  die();
}
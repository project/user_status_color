<?php
/**
 * @file
 * User status views
 */

class user_status_color_handler_field_status extends views_handler_field {

  function query() {
    $this->field_alias = 'status';
    $this->query->add_field('user_status_color', $this->field_alias);
  }
  
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  function render($values) {
    $status = $values->user_status_color_status;
    if (is_null($status)) {
      if ($values->uid == 0) {
        return "-";
      }
      else {
        $status = "Black";
        user_status_color_set_status($values->uid, $status, t("Default"), FALSE);
      }
    }
    $statuses = variable_get('user_status_color_list', array());
    $markup = "<div class = 'user_status_color_color' style = 'background-color:" . @$statuses[$status]['color'] . ";width:100%;height:100%;'>&nbsp;</div>";
    return $markup;
  }
}

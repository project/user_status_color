<?php
/**
 * @file
 * User status views
 */

class user_status_color_handler_filter_status extends views_handler_filter {
  var $no_single = TRUE;
  function option_definition() {
    $options = array();
    $options['operator'] = array('default' => 'IN');
    return $options;
  }

  function operators() {
    $operators = array(
      'IN' => array(
        'title' => t('Is one of'),
        'short' => t('in'),
        'method' => 'op_equal',
        'values' => 1,
      ),
      'NOT IN' => array(
        'title' => t('Is not one of'),
        'short' => t('not in'),
        'method' => 'op_equal',
        'values' => 1,
      ),
    );
    return $operators;
  }

  function operator_options($which = 'title') {
    $options = array();
    foreach ($this->operators() as $id => $info) {
      $options[$id] = $info[$which];
    }

    return $options;
  }

  function admin_summary() {
    if (!empty($this->options['exposed'])) {
      return t('exposed');
    }

    $values=array();
    foreach ($this->value as $k => $v) {
      if (strlen((string)$v) > 1) {
        $values[] = $v;
      }
    }
    
    $options = $this->operator_options('short');
    $output = check_plain($options[$this->operator]);
    if (in_array($this->operator, $this->operator_values(1))) {
      $output .= ' (' . implode(",", $values) . ') ';
    }
    return $output;
  }

  function operator_values($values = 1) {
    $options = array();
    foreach ($this->operators() as $id => $info) {
      if (isset($info['values']) && $info['values'] == $values) {
        $options[] = $id;
      }
    }
    return $options;
  }

  function value_form(&$form, &$form_state) {
    $form['value']['#type'] = 'checkboxes';
    $form['value']['#options'] = user_status_color_get_options(TRUE, TRUE);
    $values = array();
    foreach ($this->value as $k => $v) {
      if (strlen((string)$v) > 1) {
        $values[] = $v;
      }
    }
    $form['value']['#default_value'] = $values;
  }

  /**
   * Add this filter to the query.
   */
  function query() {
    $this->ensure_my_table();
    $field = "$this->table_alias.$this->real_field";
    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      $this->{$info[$this->operator]['method']}($field);
    }
  }

  function op_equal($field) {
    foreach ($this->value as $k => $v) {
      if ($v != '0') {
        $this->value[$k] = "'" . db_escape_string($v) . "'";
      }
      else {
        unset($this->value[$k]);
      }
    }
    if (array_search("'any'", $this->value) === FALSE) {
      $condition = "$field $this->operator (" . implode(",", $this->value) . ")";
      if (array_search("'Black'", $this->value) !== FALSE) {
        $condition = "(" . $condition . " OR $field IS" . ($this->operator == "NOT IN"?" NOT":"") . " NULL)";
      }
      $this->query->add_where($this->options['group'], $condition);
    }
  }
}
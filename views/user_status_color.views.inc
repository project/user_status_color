<?php
/**
 * @file
 * User status views
 */

function user_status_color_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'user_status_color') .'/views',
    ),
    'handlers' => array(
      'user_status_color_handler_field_status' => array(
        'parent' => 'views_handler_field',
      ),
      'user_status_color_handler_field_message' => array(
        'parent' => 'views_handler_field',
      ),
      'user_status_color_handler_field_timestamp' => array(
        'parent' => 'views_handler_field',
      ),
      'user_status_color_handler_filter_status' => array(
        'parent' => 'views_handler_filter',
      ),
    ),
  );
}

function user_status_color_views_data() {

  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['user_status_color']['table']['group'] = t('User status');

  // Define this as a base table. In reality this is not very useful for
  // this table, as it isn't really a distinct object of its own, but
  // it makes a good example.
  $data['user_status_color']['table']['base'] = array(
    'field' => 'uid',
    'title' => t('User status'),
    'help' => t("User status contains example content and can be related to users."),
    'weight' => -10,
  );

  // This table references the {user} table.
  // This creates an 'implicit' relationship to the user table, so that when 'user'
  // is the base table, the fields are automatically available.
  $data['user_status_color']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );

  // Next, describe each of the individual fields in this table to Views. For
  // each field, you may define what field, sort, argument, and/or filter
  // handlers it supports. This will determine where in the Views interface you
  // may use the field.

  // user ID field.
  $data['user_status_color']['uid'] = array(
    'title' => t('User ID'),
    'help' => t('The user id in the User status information table.'),
    // Because this is a foreign key to the {user} table. This allows us to
    // have, when the view is configured with this relationship, all the fields
    // for the related user available.
    'relationship' => array(
      'base' => 'users',
      'field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('User status'),
    ),
  );

  $data['user_status_color']['status'] = array(
    'title' => t('Status name/color'),
    'help' => t('The user status name, which nominates a color and default message (see User management -> User Status section for status names).'),
    'field' => array(
      'handler' => 'user_status_color_handler_field_status',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'user_status_color_handler_filter_status',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  $data['user_status_color']['message'] = array(
    'title' => t('Status message'),
    'help' => t('User status message, or the default one if none specified.'),
    'field' => array(
      'handler' => 'user_status_color_handler_field_message',
      'click sortable' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['user_status_color']['timestamp'] = array(
    'title' => t('Status Timestamp'),
    'help' => t('The date/time of the last user status change.'),
    'field' => array(
      'handler' => 'user_status_color_handler_field_timestamp',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  return $data;
}

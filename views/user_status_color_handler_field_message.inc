<?php
/**
 * @file
 * User status views
 */

class user_status_color_handler_field_message extends views_handler_field {

  function query() {
    $this->field_alias = 'message';
    $this->query->add_field('user_status_color', $this->field_alias);
  }
  
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  function render($values) {
    $message = $values->user_status_color_message;
    if (is_null($message)) {
      if ($values->uid == 0) {
        $message = "-";
      }
      else {
        $message = t("Default");
      }
    }
    return $message;
  }
}
